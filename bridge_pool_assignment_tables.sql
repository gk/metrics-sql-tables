CREATE TABLE IF NOT EXISTS bridge_pool_assignments_file(
   published                 TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   header                    TEXT                          NOT NULL,
   digest                    TEXT                          NOT NULL,
   PRIMARY KEY(digest)
);

CREATE index ON bridge_pool_assignments_file(published);

CREATE TABLE IF NOT EXISTS bridge_pool_assignment(
   published                 TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   digest                    TEXT                          NOT NULL,
   fingerprint               TEXT                          NOT NULL,
   distribution_method       TEXT                          NOT NULL,
   transports                TEXT,
   ip                        TEXT,
   blocklist                 TEXT,
   bridge_pool_assignments   TEXT references bridge_pool_assignments_file(digest),
   PRIMARY KEY(digest, published, fingerprint)
);

CREATE index ON bridge_pool_assignment(published);
CREATE index ON bridge_pool_assignment(fingerprint);
CREATE INDEX bridge_pool_assignment_fingerprint_published_desc_index ON bridge_pool_assignment (fingerprint, published DESC);
